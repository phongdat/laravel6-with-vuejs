<?php
namespace App\Support;

trait ApiResponseTrait {

    protected function returnSuccess($data = null) {
        $return['success'] = 1;
        if(!empty($data)) $return['model'] = $data;
        return response()
            ->json($return);
    }

}