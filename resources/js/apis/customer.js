import {apiRequest} from "../helpers/apiRequest";
import {apiMethod} from "../constants/apiMethod";


export const showCustomer = (id) => {
    return new Promise(
        (resolve, reject) => {
            apiRequest('/api/customer/' + id)
                .then(res => resolve(res))
                .catch(function (error) {
                    reject(error)
                })
        }
    )
}

export const deleteCustomer = (id) => {
    return new Promise(
        (resolve, reject) => {
            apiRequest('/api/customer/' + id, apiMethod.delete)
                .then(res => resolve(res))
                .catch(function (error) {
                    reject(error)
                })
        }
    )
}

export const createCustomer = (formData) => {
    return new Promise(
        (resolve, reject) => {
            apiRequest('/api/customer', apiMethod.post, formData)
                .then(res => resolve(res))
                .catch(function (error) {
                    reject(error)
                })
        }
    )
}

export const formEditCustomer = (id) => {
    return new Promise(
        (resolve, reject) => {
            apiRequest('/api/customer/' + id + '/edit')
                .then(res => resolve(res))
                .catch(function (error) {
                    reject(error)
                })
        }
    )
}

export const updateCustomer = (formData) => {
    return new Promise(
        (resolve, reject) => {
            apiRequest('/api/customer/' + formData.id, apiMethod.put, formData)
                .then(res => resolve(res))
                .catch(function (error) {
                    reject(error)
                })
        }
    )
}