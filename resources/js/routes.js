import CustomerList from './components/customer/ListComponent';
import InvoiceList from './components/invoice/ListComponent';
import CustomerShow from './components/customer/ShowComponent';
import CustomerForm from './components/customer/FormComponent';

const routes = [
    {path:'/invoice', name: 'invoiceList', component: InvoiceList},
    {path: '/customer', name: 'customerList', component: CustomerList},
    {path: '/customer/show/:id', name: 'customerShow', component: CustomerShow},
    {path: '/customer/create', name: 'customerCreate', component: CustomerForm},
    {path: '/customer/:id/edit', name: 'customerEdit', component: CustomerForm, meta: {mode: 'edit'}}
];

export default routes;